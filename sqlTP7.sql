START TRANSACTION;
DROP DATABASE IF EXISTS admin;
CREATE DATABASE admin CHARACTER SET utf8 COLLATE utf8_general_ci;
USE admin;

CREATE TABLE Agent (
	Code_agent	       INT AUTO_INCREMENT,
        Nom_agent	       VARCHAR(225) NOT NULL,
	Prenom_agent	       VARCHAR(225) NOT NULL,
        DateDeNaissance_agent  DATE Not NULL,
        Id_pays INT NOT NULL,
        CONSTRAINT PK_agent PRIMARY KEY (Code_agent),
        CONSTRAINT FK_agent_pays FOREIGN KEY (Id_pays) REFERENCES Pays (Id_pays)
);

CREATE TABLE Cible (
	Code_cible		       INT AUTO_INCREMENT,
    Nom_cible		       VARCHAR(225) NOT NULL,
	Prenom_cible		   VARCHAR(225) NOT NULL,
    DateDeNaissance_cible  DATE Not NULL,
    Id_pays INT NOT NULL,
    CONSTRAINT PK_cible    PRIMARY KEY (Code_cible),
    CONSTRAINT FK_cible_pays FOREIGN KEY (Id_pays) REFERENCES Pays (Id_pays)
);

CREATE TABLE Contact (
	Code_contact		     INT AUTO_INCREMENT,
    Nom_contact		         VARCHAR(225) NOT NULL,
	Prenom_contact		     VARCHAR(225) NOT NULL,
    DateDeNaissance_contact  DATE Not NULL,
    CONSTRAINT PK_contact    PRIMARY KEY (Code_contact)
);

CREATE TABLE Planque (
	Code_planque		     INT AUTO_INCREMENT,
    Address_planque		         VARCHAR(225) NOT NULL,
	Type_planque		     VARCHAR(225) NOT NULL,
        Id_pays  INT NOT NULL,
    CONSTRAINT PK_planque    PRIMARY KEY (Code_planque),
    CONSTRAINT FK_idPays FOREIGN KEY (Id_pays) REFERENCES Pays (Id_pays)
);

CREATE TABLE Mission (
	Code_mission		       INT AUTO_INCREMENT,
    Titre_mission		       VARCHAR(225) NOT NULL,
	Discription_mission		   VARCHAR(225) NOT NULL,
	DateDeput_mission		   VARCHAR(225) NOT NULL,
	DateFin_mission		   VARCHAR(225) NOT NULL,
    CONSTRAINT PK_mission    PRIMARY KEY (Code_mission)
);

CREATE TABLE Pays (
	Id_pays		       INT AUTO_INCREMENT,
    Nationalite_pays		       VARCHAR(225) NOT NULL,
    CONSTRAINT PK_pays    PRIMARY KEY (Id_pays)
);

CREATE TABLE TypeMission (
	Id_typemission		       INT AUTO_INCREMENT,
    Libelle_typemission	        VARCHAR(225) NOT NULL,
    CONSTRAINT PK_typemission    PRIMARY KEY (Id_typemission)
);

CREATE TABLE Statuss (
	Id_status		       INT AUTO_INCREMENT,
    Libelle_status	        VARCHAR(225) NOT NULL,
    CONSTRAINT PK_status    PRIMARY KEY (Id_status)
);

CREATE TABLE Specialite (
	Id_specialite		       INT AUTO_INCREMENT,
    Libelle_specialite	        VARCHAR(225) NOT NULL,
    CONSTRAINT PK_specialite    PRIMARY KEY (Id_specialite)
);

ALTER TABLE Contact
ADD CONSTRAINT FK_contact_pays FOREIGN KEY (Id_pays) REFERENCES Pays (Id_pays);

ALTER TABLE Mission
ADD COLUMN Id_pays INT NOT NULL;

ALTER TABLE Mission
ADD COLUMN Id_typemission INT NOT NULL;

ALTER TABLE Mission
ADD CONSTRAINT FK_mission_pays FOREIGN KEY (Id_pays) REFERENCES Pays (Id_pays);

ALTER TABLE Mission
ADD CONSTRAINT FK_mission_specialite FOREIGN KEY (Id_specialite) REFERENCES Specialite (Id_specialite);

ALTER TABLE Mission
ADD CONSTRAINT FK_mission_typemission FOREIGN KEY (Id_typemission) REFERENCES TypeMission (Id_typemission);


CREATE TABLE Agent_Mission (
	Code_agent	       INT NOT NULL,
        Code_mission           INT NOT NULL,
        CONSTRAINT PK_agent_mission PRIMARY KEY (Code_agent, Code_mission)
);


CREATE TABLE Cible_Mission (
	Code_cible	       INT NOT NULL,
        Code_mission           INT NOT NULL,
        CONSTRAINT PK_agent_mission PRIMARY KEY (Code_cible, Code_mission)
);

CREATE TABLE Contact_Mission (
	Code_contact	       INT NOT NULL,
        Code_mission           INT NOT NULL,
        CONSTRAINT PK_agent_mission PRIMARY KEY (Code_contact, Code_mission)
);

CREATE TABLE Statuss_Mission (
	Id_status	       INT NOT NULL,
        Code_mission           INT NOT NULL,
        CONSTRAINT PK_status_mission PRIMARY KEY (Id_status, Code_mission)
);

CREATE TABLE Specialite_Mission (
	Id_specialite	       INT NOT NULL,
        Code_mission           INT NOT NULL,
        CONSTRAINT PK_specialite_mission PRIMARY KEY (Id_specialite, Code_mission)
);

CREATE TABLE Specialite_Agent (
	Id_specialite	       INT NOT NULL,
        Code_agent           INT NOT NULL,
        CONSTRAINT PK_specialite_agent PRIMARY KEY (Id_specialite, Code_agent)
);

CREATE TABLE Planque_Mission (
	Code_planque	       INT NOT NULL,
        Code_mission           INT NOT NULL,
        CONSTRAINT PK_planque_mission PRIMARY KEY (Code_planque, Code_mission)
);


 CREATE TABLE Adminstrators (
	    Id_admin	       INT AUTO_INCREMENT,
        username           VARCHAR(225) NOT NULL,
        Prenom_admin           VARCHAR(225),
        Email_admin           VARCHAR(225),
        password          VARCHAR(225) NOT NULL,
        DateDeCreation_admin           DATE,
        CONSTRAINT PK_admin PRIMARY KEY (Id_admin)
);



INSERT INTO Pays VALUES (1, "FranÃ§ais","France");
INSERT INTO Pays VALUES (2, "Syrienne","Syrie");
INSERT INTO Pays VALUES (3, "Algerienne","Alger");
INSERT INTO Pays VALUES (4, "Colombienne","Colombia");

SELECT * FROM Pays;


INSERT INTO Agent VALUES (1, "Emilie","Cesco-resia","2000-01-01", 1);
INSERT INTO Agent VALUES (2, "Majd","Alhafez","1989-05-05", 2);
INSERT INTO Agent VALUES (3, "Julie","Souchet","2001-02-02", 1);
INSERT INTO Agent VALUES (4, "Estefania","Vila", "2002-03-03", 4);
INSERT INTO Agent VALUES (5, "Cedric","Kadima","2003-04-04", 1);
INSERT INTO Agent VALUES (6, "Mickael","Vandenbulcke","1989-06-06", 1);
INSERT INTO Agent VALUES (7, "Nadjib","Chaibeddra","1993-07-07", 3);


SELECT * FROM Agent;

INSERT INTO Cible VALUES (1, "Adam","Show","2000-01-01", 1);
INSERT INTO Cible VALUES (2, "Tom","Mac","1989-05-05", 2);
INSERT INTO Cible VALUES (3, "Dom","Eli","2001-02-02", 1);
INSERT INTO Cible VALUES (4, "Eric","Vila", "2002-03-03", 4);


SELECT * FROM Cible;

INSERT INTO Contact VALUES (1, "Tom","Show","2000-01-01", 1);
INSERT INTO Contact VALUES (2, "Adam","Hafez","1989-05-05", 3);
INSERT INTO Contact VALUES (3, "Dom","Eli","2001-02-02", 2);
INSERT INTO Contact VALUES (4, "Eric","Via", "2002-03-03", 4);


SELECT * FROM Contact;

INSERT INTO Planque VALUES (1, "Syrie","Appartmement", 2);
INSERT INTO Planque VALUES (2, "France","Maison", 1);
INSERT INTO Planque VALUES (3, "Colombia","Appartmement", 3);
INSERT INTO Planque VALUES (4, "Alger","Maison", 4);


SELECT * FROM Planque;

INSERT INTO TypeMission VALUES (1, "Surveillance");
INSERT INTO TypeMission VALUES (2, "Assassinat");
INSERT INTO TypeMission VALUES (3, "Infiltration");


SELECT * FROM TypeMission;

INSERT INTO Statuss VALUES (1, "En preparation");
INSERT INTO Statuss VALUES (2, "en cours, terminÃ©");
INSERT INTO Statuss VALUES (3, "echec");


SELECT * FROM Statuss;

INSERT INTO Specialite VALUES (1, "Expert Informatique");
INSERT INTO Specialite VALUES (2, "Psycologue");
INSERT INTO Specialite VALUES (3, "Expert en arme");


SELECT * FROM Specialite;

INSERT INTO Misiion VALUES (1, "Premiere prioritÃ©");
INSERT INTO Misiion VALUES (2, "DeuxiÃ¨me prioritÃ©");
INSERT INTO Misiion VALUES (3, "TroisiÃ¨me prioritÃ©");


SELECT * FROM Specialite;

INSERT INTO Mission VALUES (1, "Premiere prioritÃ©", "Killer", "2012-01-04", "2013-05-06", 1, 1, 2);
INSERT INTO Mission VALUES (2, "DeuxiÃ¨me prioritÃ©", "Grand voler", "2010-05-04", "2013-06-05", 3, 2 , 3);
INSERT INTO Mission VALUES (3, "TroisiÃ¨me prioritÃ©", "Petit voler", "2015-01-04", "2015-05-06", 2, 3, 1);


SELECT * FROM Mission;

INSERT INTO Adminstrators VALUES (1, 'MM', 'HH', 'dfd@fgf', 'wewew', '2020-09-09');

INSERT INTO Adminstrators (Nom_admin, Password_admin) VALUES ('MA', 'errer');

DELETE FROM Adminstrators
WHERE Id_admin = 5
OR Id_admin = 4
OR Id_admin = 3
OR Id_admin = 2
OR Id_admin = 1;

 INSERT INTO Adminstrators (username, password) VALUES ('admin', 'admin');
 
 SELECT Titre_mission, Discription_mission, DateDeput_mission, DateFin_mission, Nationalite_pays, Nom_pays, Libelle_specialite, Libelle_typemission FROM admin.Mission
JOIN admin.Pays ON Pays.Id_pays = Mission.Id_pays
JOIN admin.Specialite ON Specialite.Id_specialite = Mission.Id_specialite
JOIN admin.TypeMission ON TypeMission.Id_typemission = Mission.Id_typemission
WHERE Titre_mission = 'Premiere prioritÃ©';


COMMIT;