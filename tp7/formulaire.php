<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="./assets/style.css">
</head>
<body>

<div class="container">
  <div class="row"> 
     
    <div class="col-md-6 col-sm-12 para" id="div-click-mission">
    <h1 class="grand-titre mb-4">Veuillez cliquer dessus pour générer le <span>formulaire</span></h1>
      <p class="p-style" id="creer-mission">Créer ta propre mission</p>
      <p class="p-style">Ajouter un agent</p>
      <p class="p-style">Ajouter une cible</p>
      <p class="p-style">Ajouter un contact</p>
      <p class="p-style">Ajouter une planque</p>
    </div>

    <div class="col-md-6 col-sm-12" id="div-click-formulaire">
    <div class="main-content">
            <form>
              <h1 class="mb-3">Créer votre propre mission</h1>
           <div class="form-group">
             <label for="exampleFormControlInput1">Mission</label>
             <input type="text" class="form-control" id="nom-mission" placeholder="Entrez le nom de votre mission" name="Titre_mission">
            </div>

            <div class="form-row">
            <div class="form-group col-md-6 col-sm-12">
      <label for="inputState">Nom de l'agent</label>
      <select id="inputState" class="form-control" name="Nom_agent">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label for="inputState">Nationalité de l'agent</label>
      <select id="inputState" class="form-control" name=" Id_pays">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label for="inputContact">Nom de la cible</label>
      <select id="inputContact" class="form-control" name="Nom_contact">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label for="inputContact">Nationalité de la cible</label>
      <select id="inputContact" class="form-control" name=" Id_pays">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
  </div>

  <div class="form-row">
            <div class="form-group col-md-12">
      <label for="inputState">Type de mission</label>
      <select id="inputState" class="form-control" name="Libelle_typemission">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-grou col-md-12">
             <label for="exampleFormControlInput1">Description mission</label>
             <input type="text" class="form-control" id="descrip-mission" placeholder="Entrer la description de la mission" name="Discription_mission">
            </div>

    <div class="form-group col-md-12">
      <label for="exampleFormControlInput1">Adresse de la planque</label>
      <input type="text" class="form-control" id="adresse-planque" placeholder="adresse de la planque" name="Address_planque">
    </div>

   
    <div class="form-group col-md-6">
      <label for="inputContact">Type de planque</label>
      <select id="type-planque" class="form-control" name=" Type_planque">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label for="inputContact">Pays de la planque</label>
      <select id="inputContact" class="form-control" name=" Id_pays">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class=" form-group col-md-12">
    <input class="input-envoyer mt-3" type="submit" id='submit' value='ENVOYER' name="envoyer">
    </div>

  </div>

            </form>

    </div>
    

    </div>

    <div class="div">
        <form class="form-content">
      <p></p>
    </form>
    </div>
  
  </div>

          
    
</div>
<script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
        <script src="https://unpkg.com/typewriter-effect@latest/dist/core.js"></script>

  <script src="index2.js"></script>
</body>
</html>