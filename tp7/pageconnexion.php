<?php  
 session_start();  
 
 try  
 {  
      $connect = new PDO('mysql:localhost;port=3306;dname=admin1,charset=utf8;' , 'admin' , 'admin');
      $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      if(isset($_POST["login"]))  
      {  
           if(empty($_POST["username"]) || empty($_POST["password"]))  
           {  
                $message = '<label>All fields are required</label>';  
           }  
           else  
           {  
                $query = "SELECT * FROM admin1.Adminstrators WHERE username = :username AND password = :password";  
                $statement = $connect->prepare($query);  
                $statement->execute(  
                     array(  
                          'username'     =>     $_POST["username"],  
                          'password'     =>     $_POST["password"]  
                     )  
                );  
                $count = $statement->rowCount();  
                if($count > 0)  
                {  
                     $_SESSION["username"] = $_POST["username"];  
                     header("location:verification.php");  
                }  
                else  
                {  
                     $message = "<a style='color:red;'>" . 'Wrong Data' . "</a>";
                }  
           }  
      }  
 }  
 catch(PDOException $error)  
 {  
      $message = $error->getMessage();  
 }  
 ?> 

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Connexion</title>
  <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:wght@300&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="./assets/style.css">
</head>

<body>

  <div class="container-fluid d-flex">
    <div class="formulaire-co row justify-content-center">
      
      <?php
      if(isset($message))  
      {  
           echo '<label class="text-danger">'.$message.'</label>';  
      }  
     ?>  

    <h3></h3><br />  

      <form method="POST">


        <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>


        <input type="password" placeholder="Entrer le mot de passe" name="password" required>

        <input type="submit" id='submit' value='LOGIN' name="login">

      
      </form>
    </div>
  </div>


</body>

</html>
