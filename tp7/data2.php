<?php

require './Model/db.php';

$req1 = $connexion->query('SELECT Nom_agent, Nationalite_pays FROM admin.Agent
JOIN admin.Pays ON Pays.Id_pays = Agent.Id_pays');

$req2 = $connexion->query('SELECT Nom_Cible, Nationalite_pays FROM admin.Cible
JOIN admin.Pays ON Pays.Id_pays = Cible.Id_pays');

$data1 = array();
$data2 = array();
while ($row1 = $req1->fetch())
{
    $data1[] = $row1;
}

header('Content-type:application/json;charset=utf-8');
echo json_encode($data1);

while ($row2 = $req2->fetch())
{
    $data2[] = $row2;
}

//print_r($data1, $data2);

header('Content-type:application/json;charset=utf-8');
echo json_encode($data2);

?>